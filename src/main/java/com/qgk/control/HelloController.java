package com.qgk.control;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HelloController {
    // 浏览器输入http://localhost:8080/hello
    @ResponseBody
    @RequestMapping("/hello")
    public String hello(){
        return "hello world" +
                "";
    }
}


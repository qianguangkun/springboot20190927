package com.qgk.qgk;
//反射调用类中的setter/getter方法

import java.lang.reflect.Method;

class Member {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}



public class JavaReflectDemo {

    public static void main(String[] args) throws Exception{
        Class<?>  cls = Member.class;
        String value = "需补发";
        //通过反射实例化才可以调用类中的成员属性及方法
        //调用无参构造 实例化
        Object object = cls.getDeclaredConstructor().newInstance();
        //反射调用方法 需要明确知道方法中的参数    // 类型

        Method setName = cls.getDeclaredMethod("setName", String.class);

        setName.invoke(object, value);

        Method getName = cls.getDeclaredMethod("getName");

        System.out.println(getName.invoke(object));

    }
}
